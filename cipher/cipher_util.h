#ifndef INCLUDED_CIPHER_UTIL_H
#define INCLUDED_CIPHER_UTIL_H

namespace cipher {
namespace ERRNO {

    enum ERRNO: const int
    {
        SUCCESS = 0,
        OPTION = 1,
        RUNTIME = 2
    };

} //

namespace util {
    // negated full int bits => -1
    // forcing this to unsigned and right-shifting by 1bit returns max int
    const int max_int = (((unsigned int)(~(int) 0)) >> 1);
    const int min_int = ~(max_int -1);
} //
} //
#endif
